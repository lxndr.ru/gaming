import Image from 'next/image'

export function BuyMeACoffee() {
  return (
    <a href="https://www.buymeacoffee.com/lxndr" target="_blank" rel="noreferrer">
      <Image
        width={217}
        height={60}
        src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
        alt="Buy Me A Coffee"
      />
    </a>
  )
}
