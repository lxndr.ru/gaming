import type { Item } from '../types'
import { IngredientItem } from './IngredientItem'

interface IngredientListProps {
  items: Item[]
}

export function IngredientList({ items }: IngredientListProps) {
  return (
    <ul className="sticky top-0">
      {items.map(item => (
        <IngredientItem
          key={item.id}
          item={item}
        />
      ))}
    </ul>
  )
}
