import Image from 'next/image'
import type { Item } from '../types'

interface IngredientItemProps {
  item: Item
}

export function IngredientItem({
  item: { title, iconFileHash, mod, tag, price, weight },
}: IngredientItemProps) {
  return (
    <li className="flex gap-2 rounded p-2 hover:bg-slate-200">
      {iconFileHash &&
        <div className="w-[24px] h-[24px] shrink-0">
          <Image
            className="w-full h-full object-contain"
            width={24}
            height={24}
            src={`http://localhost:3000/api/files/${iconFileHash}`}
            alt={title}
            title={title}
          />
        </div>
      }

      <div className="flex flex-col grow gap-1">
        <div>{title}</div>
        <div className="flex gap-2 items-center justify-end">
          {tag === 'quest' && (
            <span className="px-1 rounded bg-blue-500 text-white" title="Quest item">Q</span>
          )}
          {mod.id !== 'base' && (
            <span className="px-1 rounded bg-green-500 text-white" title={mod.title}>{mod.abbr}</span>
          )}
          {!!price && (
            <div className="w-12">
              <span className="inline-block w-[12px] h-[12px] bg-contain bg-[url('/images/common/coin.png')]" />
              <span className="ml-1">{price.toString()}</span>
            </div>
          )}
          {!!weight && (
            <div className="w-12">
              <span className="inline-block w-[12px] h-[12px] bg-contain bg-[url('/images/common/weight.png')]" />
              <span className="ml-1">{weight}</span>
            </div>
          )}
        </div>
      </div>
    </li>
  )
}
