import { cache } from 'react'
import { prisma } from '@lxndr/gaming-db'
import { GameData } from './types'

export const fetchAlchemyData = cache(async (gameId: string): Promise<GameData> =>
  await prisma.game.findUniqueOrThrow({
    where: { id: gameId },
    include: {
      mods: {
        orderBy: {
          order: 'asc',
        },
      },
      items: {
        include: {
          mod: true,
          effects: {
            include: {
              effect: true,
            },
            orderBy: {
              order: 'asc',
            },
          },
        },
      },
    },
  })
)
