import { useMemo } from 'react'
import { pipe, map, flatten, filter, pathEq, sortBy, uniqBy, prop, indexBy, values } from 'ramda'
import type { GameData, Item, ItemEffect } from './types'

const filterByEffect = (effectId: string) =>
  filter<Item>(item =>
      item.effects.some(pathEq(effectId, ['effect', 'id'])))

interface UseAlchemyData {
  gameData: GameData
  modIds: string[]
  effectId?: string
}

export function useAlchemyData({ gameData, modIds, effectId }: UseAlchemyData) {
  const items = useMemo(
    () =>
      pipe(
        map(modId => filter(pathEq(modId, ['mod', 'id']), gameData.items)),
        flatten,
        indexBy(prop('id')),
        values,
        sortBy(prop('title')),
      )(modIds),
    [gameData, modIds],
  )

  const filteredItems = useMemo(() => {
    let result = items

    if (effectId) {
      result = filterByEffect(effectId)(result)
    }

    return result
  }, [items, effectId])

  const effects = useMemo(
    () =>
      pipe(
        map<Item, ItemEffect[]>(prop('effects')),
        flatten,
        map(prop('effect')),
        uniqBy(prop('id')),
        sortBy(prop('title')),
      )(items),
    [items],
  )

  return {
    effects,
    filteredItems,
  }
}
