import { Header } from '~/components/Header'
import { ModList } from '~/components/ModList'
import { RawSearchParams, parseModsSearchParam, parseStringSearchParam } from '~/utils/searchParams'
import { EffectList } from './EffectList'
import { IngredientList } from './IngredientList'
import { useAlchemyData } from './useAlchemyData'
import { GameData } from './types'

interface AlchemyScreenProps {
  gameData: GameData
  searchParams: RawSearchParams
}

export function AlchemyScreen({ gameData, searchParams }: AlchemyScreenProps) {
  const modIds = parseModsSearchParam(searchParams.mods, gameData.mods)
  const effectId = parseStringSearchParam(searchParams.effect)

  const { effects, filteredItems } = useAlchemyData({
    gameData,
    modIds,
    effectId,
  })

  return (
    <>
      <Header gameTitle={gameData.title}>
        <ModList
          availableMods={gameData.mods}
          selectedModIds={modIds}
          searchParams={searchParams}
        />
      </Header>

      <div className="flex divide-x divide-dashed divide-slate-300 sm:max-w-xl sm:mx-auto">
        <div className="p-1 w-full">
          <EffectList
            effects={effects}
            selected={effectId}
            searchParams={searchParams}
          />
        </div>
        <div className="p-1 w-full">
          <IngredientList
            items={filteredItems}
          />
        </div>
      </div>
    </>
  )
}
