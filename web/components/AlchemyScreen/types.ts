import type {
  Item as DbItem,
  ItemEffect as DbItemEffect,
  Effect as DbEffect,
  Game as DbGame,
} from '@prisma/client'
import type { Mod } from '@lxndr/gaming-db'

export interface Effect extends Pick<DbEffect, 'id' | 'title' | 'iconFileHash'> {}

export interface ItemEffect extends Pick<DbItemEffect, 'id' | 'magnitude' | 'duration'> {
  effect: Effect
}

export interface Item extends Pick<DbItem, 'id' | 'title' | 'description' | 'iconFileHash' | 'tag' | 'price' | 'weight'> {
  mod: Mod
  effects: ItemEffect[]
}

export interface GameData extends DbGame {
  mods: Mod[]
  items: Item[]
}
