import type { RawSearchParams } from '~/utils/searchParams'
import type { Effect } from '../types'
import { EffectItem } from './EffectItem'

interface EffectListProps {
  effects: Effect[]
  selected?: string
  searchParams: RawSearchParams
}

export function EffectList({ effects, selected, searchParams }: EffectListProps) {
  return (
    <ul className="">
      {effects.map((effect) => (
        <EffectItem
          key={effect.id}
          effect={effect}
          selected={effect.id === selected}
          searchParams={searchParams}
        />
      ))}
    </ul>
  )
}
