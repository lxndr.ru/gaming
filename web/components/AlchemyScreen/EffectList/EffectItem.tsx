import Link from 'next/link'
import Image from 'next/image'
import cn from 'classnames'
import type { RawSearchParams } from '~/utils/searchParams'
import type { Effect } from '../types'

interface EffectItemProps {
  effect: Effect
  selected: boolean
  searchParams: RawSearchParams
}

export function EffectItem({
  effect: { id, title, iconFileHash },
  selected,
  searchParams,
}: EffectItemProps) {
  return (
    <li
      className={cn(
        'rounded hover:bg-slate-200',
        { ['bg-amber-300 hover:bg-amber-300']: selected },
      )}
    >
      <Link className="flex gap-2 p-2" href={{ query: { ...searchParams, effect: id } }}>
        <div className="w-[24px] h-[24px] shrink-0">
          {iconFileHash &&
            <Image
              className="w-full h-full object-contain"
              width={24}
              height={24}
              src={`http://localhost:3000/api/files/${iconFileHash}`}
              alt={title}
              title={title}
            />
          }
        </div>
        <div>{title}</div>
      </Link>
    </li>
  )
}
