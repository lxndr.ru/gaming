import { ReactNode } from 'react'
import Link from 'next/link'

interface HeaderProps {
  gameTitle: string
  children: ReactNode
}

export function Header({ gameTitle, children }: HeaderProps) {
  return (
    <nav className="flex flex-col gap-2 p-2 sm:flex-row sm:items-center sm:justify-start">
      <div className="flex justify-between sm:flex-row sm:gap-2">
        <Link
          className="text-blue-500 hover:text-blue-700"
          href="/"
        >
          To the list
        </Link>

        <div className="">{gameTitle}</div>
      </div>
  
      <div>
        {children}
      </div>
    </nav>
  )
}
