import Link from 'next/link'
import { formatSeconds } from '~/utils/formatSeconds'
import type { ItemEffect } from './types'
import { RawSearchParams } from '~/utils/searchParams'

interface EffectProps {
  effect: ItemEffect
  searchParams: RawSearchParams
}

export function Effect({
  effect: { effect: { title }, magnitude, duration },
  searchParams,
}: EffectProps) {
  return (
    <div>
      &bull;&nbsp;

      <Link
        shallow
        className="hover:underline"
        href={{ query: { ...searchParams, search: title } }}
      >
        {title}
      </Link>

      {magnitude && (
        <span> {magnitude}</span>
      )}

      {duration && (
        <span> for {formatSeconds(duration)}</span>
      )}
    </div>
  )
}
