import Link from 'next/link'
import Image from 'next/image'
import type { RawSearchParams } from '~/utils/searchParams'
import type * as types from './types'

interface ItemProps {
  item: types.Item
  quantity: number | null
  searchParams: RawSearchParams
}

export function Item({ item: { title, iconFileHash }, quantity, searchParams }: ItemProps) {
  return (
    <div>
      <Link shallow className="group" href={{ query: { ...searchParams, search: title } }}>
        <span className="inline-block w-[24px] h-[24px] align-middle">
          {iconFileHash &&
            <Image
              className="object-contain w-full h-full"
              width={24}
              height={24}
              src={`http://localhost:3000/api/files/${iconFileHash}`}
              alt={title}
            />
          }
        </span>
        &nbsp;
        <span className="group-hover:underline align-middle">
          {title}
        </span>
      </Link>

      {quantity != null && quantity !== 1 && (
        <span className="align-middle">&nbsp;(x{quantity})</span>
      )}
    </div>
  )
}
