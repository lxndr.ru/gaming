import { Header } from '~/components/Header'
import { ModList } from '~/components/ModList'
import { RawSearchParams, parseModsSearchParam, parseStringSearchParam } from '~/utils/searchParams'
import { RawGameData} from './fetchCraftingData'
import { useCraftingData } from './useCraftingData'
import { Filter, FilterBar } from './FilterBar'
import { Row } from './Row'

interface CraftingScreenProps {
  gameData: RawGameData
  searchParams: RawSearchParams
}

export function CraftingScreen({ gameData, searchParams }: CraftingScreenProps) {
  const modIds = parseModsSearchParam(searchParams.mods, gameData.mods)
  const category = parseStringSearchParam(searchParams.category)
  const search = parseStringSearchParam(searchParams.search)

  const { categories, filteredRecipes, columns } = useCraftingData({
    gameData,
    modIds,
    categoryId: category,
    search,
  })

  return (
    <div>
      <Header gameTitle={gameData.title}>
        <ModList
          availableMods={gameData.mods}
          selectedModIds={modIds}
          searchParams={searchParams}
        />
      </Header>

      <FilterBar
        categories={categories}
        initialValues={{ category, search }}
      />

      <table className="w-full" id="item-list">
        <thead>
          <tr className="border-b border-slate-300">
            <th className="text-left">Item</th>
            {columns.effects &&
              <th className="text-left hidden sm:table-cell">Effects</th>}
            <th className="text-left">Components</th>
            {columns.requirements &&
              <th className="text-left hidden sm:table-cell">Requirements</th>}
            {columns.category &&
              <th className="text-left hidden sm:table-cell">Category</th>}
          </tr>
        </thead>
        <tbody className="divide-y divide-dashed divide-slate-300">
          {filteredRecipes.map(recipe => (
            <Row key={recipe.id} recipe={recipe} columns={columns} searchParams={searchParams} />
          ))}
        </tbody>
      </table>
    </div>
  )
}
