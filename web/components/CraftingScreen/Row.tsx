import Link from 'next/link'
import type { RawSearchParams } from '~/utils/searchParams'
import { ComponentList } from './ComponentList'
import { Effect } from './Effect'
import { Item } from './Item'
import type { Recipe } from './types'

interface RowProps {
  recipe: Recipe
  columns: {
    effects?: boolean
    requirements?: boolean
    category?: boolean
  }
  searchParams: RawSearchParams
}

export function Row({
  recipe: { item, quantity, category, components, requirements },
  columns,
  searchParams,
}: RowProps) {
  return (
    <tr>
      <td className="p-2 align-top">
        <Item item={item} quantity={quantity} searchParams={searchParams} />
      </td>

      {columns.effects && (
        <td className="p-2 align-top hidden sm:table-cell">
          {item.effects.map(effect => (
            <Effect key={effect.id} effect={effect} searchParams={searchParams} />
          ))}
        </td>
      )}

      <td className="p-2 align-top">
        <ComponentList list={components} searchParams={searchParams} />
      </td>

      {columns.requirements && (
        <td className="p-2 align-top hidden sm:table-cell">
          {requirements.map(({ requirement: { title }, operator, value }) => {
            const text = [title, operator, value].filter(Boolean).join(' ')
            return <div key={text}>&bull; {text}</div>
          })}
        </td>
      )}

      {columns.category && (
        <td className="p-2 align-top hidden sm:table-cell">
          {category && (
            <Link
              shallow
              className="hover:underline"
              href={{ query: { ...searchParams, category: category.id } }}
            >
              {category.title}
            </Link>
          )}
        </td>
      )}
    </tr>
  )
}
