import { cache } from 'react'
import { Category, Effect, Game, Item, ItemEffect, Mod, Recipe, RecipeComponent, RecipeRequirement, Requirement } from '@prisma/client'
import { prisma } from '@lxndr/gaming-db'

export interface RawGameData extends Game {
  mods: Mod[]
  items: (Item & {
    mod: Mod
    effects: (ItemEffect & {
      effect: Effect
    })[]
  })[]
  recipes: (Recipe & {
    mod: Mod
    category: Category | null
    requirements: (RecipeRequirement & {
      requirement: Requirement
    })[]
    components: RecipeComponent[]
  })[]
}

export const fetchCraftingData = cache(async (gameId: string): Promise<RawGameData> =>
  await prisma.game.findUniqueOrThrow({
    where: { id: gameId },
    include: {
      mods: {
        orderBy: {
          order: 'asc',
        },
      },
      items: {
        include: {
          mod: true,
          effects: {
            include: {
              effect: true,
            },
            orderBy: {
              order: 'asc',
            },
          },
        },
      },
      recipes: {
        include: {
          mod: true,
          category: true,
          requirements: {
            include: {
              requirement: true,
            },
          },
          components: {
            orderBy: {
              order: 'asc',
            },
          },
        },
      },
    },
  })
)
