import Link from 'next/link'
import { CategorySelect } from './CategorySelect'
import { Category } from './types'

export interface Filter {
  category?: string
  search?: string
}

interface FilterBarProps {
  categories: Category[]
  initialValues: Filter
}

export function FilterBar({
  categories,
  initialValues,
}: FilterBarProps) {
  return (
    <form
      id="filter"
      className="flex flex-wrap sm:flex-nowrap gap-4 m-2 justify-center"
      method="GET"
    >
      {!!categories.length && (
        <CategorySelect
          categories={categories}
          defaultValue={initialValues.category}
        />
      )}

      <input
        className="border rounded px-2 py-1"
        placeholder="Search"
        name="search"
        defaultValue={initialValues.search}
      />

      <div className="flex gap-2">
        <button
          id="search-btn"
          type="submit"
          className="border rounded px-4 py-1 bg-lime-600 text-white"
        >
          Search
        </button>

        <Link
          id="clear-btn"
          className="bg-white border rounded px-4 flex items-center"
          href="?"
        >
          Clear
        </Link>
      </div>
    </form>
  )
}
