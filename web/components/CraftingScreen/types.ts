import type {
  Mod as DbMod,
  Category as DbCategory,
  Effect as DbEffect,
  Requirement as DbRequirement,
  Item as DbItem,
  ItemEffect as DbItemEffect,
  RecipeComponent as DbRecipeComponent,
  RecipeRequirement as DbRecipeRequirement,
  Recipe as DbRecipe,
} from '@prisma/client'

export interface Mod extends Pick<DbMod, 'id' | 'title' | 'abbr'> {}

export interface Category extends Pick<DbCategory, 'id' | 'title'> {}

export interface Effect extends Pick<DbEffect, 'id' | 'title' | 'iconFileHash'> {}

export interface ItemEffect extends Pick<DbItemEffect, 'id' | 'magnitude' | 'duration'> {
  effect: Effect
}

export interface Item extends Pick<DbItem, 'id' | 'title' | 'description' | 'iconFileHash' | 'tag' | 'price' | 'weight'> {
  effects: ItemEffect[]
}

export interface Requirement extends Pick<DbRequirement, 'id' | 'title'> {}

export interface RecipeComponent extends Pick<DbRecipeComponent, 'id'| 'quantity'> {
  item: Item
}

export interface RecipeRequirement extends Pick<DbRecipeRequirement, 'id' | 'operator' | 'value'> {
  requirement: Requirement
}

export interface Recipe extends Pick<DbRecipe, 'id' | 'quantity'> {
  mod: DbMod
  item: Item
  category: Category | null
  components: RecipeComponent[]
  requirements: RecipeRequirement[]
  searchIndex: string[]
}
