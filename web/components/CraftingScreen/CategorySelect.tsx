import { Category } from './types'

interface CategorySelectProps {
  categories: Category[]
  defaultValue?: string
}

export function CategorySelect({ categories, defaultValue }: CategorySelectProps) {
  return (
    <select
      className="border rounded px-2 py-1 bg-white"
      name="category"
      defaultValue={defaultValue}
    >
      <option key="any" value="">Any category</option>

      {categories.map(category => (
        <option
          key={category.id}
          value={category.id}
        >
          {category.title}
        </option>
      ))}
    </select>
  )
}
