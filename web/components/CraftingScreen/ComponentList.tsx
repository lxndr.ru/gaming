import { RawSearchParams } from '~/utils/searchParams'
import { Item } from './Item'
import { RecipeComponent } from './types'

interface ComponentListProps {
  list: RecipeComponent[]
  searchParams: RawSearchParams
}

export function ComponentList({ list, searchParams }: ComponentListProps) {
  return (
    <div className="flex flex-col gap-1">
      {list.map(cmp =>
        <Item
          key={cmp.id}
          item={cmp.item}
          quantity={cmp.quantity}
          searchParams={searchParams} />
        )}
    </div>
  )
}
