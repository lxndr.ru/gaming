import { useMemo } from 'react'
import { pipe, prop, propEq, map, filter, reject, flatten, indexBy, sortBy, uniqBy, isNil } from 'ramda'
import { RawGameData } from './fetchCraftingData'
import type { Category, Recipe } from './types'

interface UseCraftingData {
  gameData: RawGameData
  modIds: string[]
  categoryId?: string
  search?: string
}

export function useCraftingData({ gameData, modIds, categoryId, search }: UseCraftingData) {
  const items = useMemo(
    () =>
      pipe(
        map(modId => filter(propEq(modId, 'modId'), gameData.items)),
        flatten,
        indexBy(prop('id')),
      )(modIds),
    [gameData, modIds],
  )

  const recipes = useMemo(() => {
    const recipes: Record<string, Recipe> = {}

    modIds.forEach(modId => {
      const modRecipes = gameData.recipes.filter(recipe => recipe.modId === modId)

      modRecipes.forEach(({ itemId, components, ...recipe }) => {
        const result: Recipe = {
          ...recipe,
          item: items[itemId],
          components: components.map(({ itemId, ...component }) => ({
            ...component,
            item: items[itemId],
          })),
          searchIndex: [],
        }

        result.searchIndex = [
            result.item.title,
            ...result.item.effects.map(({ effect }) => effect.title),
            ...result.components.map(({ item }) => item.title),
            ...result.requirements.map(({ requirement }) => requirement.title),
          ]
          .filter<string>((str): str is string => !!str)
          .map(str => str.toLocaleLowerCase())

        recipes[recipe.id] = result
      })
    })

    return Object.values(recipes)
  }, [gameData, modIds, items])

  const filteredRecipes = useMemo(() => {
    let result = recipes

    if (categoryId) {
      result = result.filter(({ category }) => category?.id === categoryId)
    }

    if (search) {
      const normalizedSearch = search.toLocaleLowerCase().trim()

      result = result.filter(({ searchIndex }) =>
        searchIndex.some(
          text => text.includes(normalizedSearch)))
    }

    return sortBy(recipe => recipe.item.title, result)
  }, [recipes, categoryId, search])

  const categories = useMemo(
    () =>
      pipe(
        map<Recipe, Category | null>(prop('category')),
        reject(isNil),
        uniqBy(prop('id')),
        sortBy<Category>(prop('title')),
      )(recipes),
    [recipes],
  )

  const columns = useMemo(() => ({
    effects: !!recipes.some(recipe => !!recipe.item.effects.length),
    requirements: recipes.some(recipe => recipe.requirements.length),
    category: !!categories.length,
  }), [recipes, categories.length])

  return {
    categories,
    filteredRecipes,
    columns,
  }
}
