import Link from 'next/link'
import cn from 'classnames'
import type { Mod } from '@lxndr/gaming-db'
import type { RawSearchParams } from '~/utils/searchParams'

interface ModListProps {
  availableMods: Mod[]
  selectedModIds: string[]
  searchParams: RawSearchParams
}

export function ModList({ availableMods, selectedModIds, searchParams }: ModListProps) {
  const available = availableMods.filter(mod => mod.id !== 'base')
  const selected = selectedModIds.filter(id => id !== 'base')

  return (
    <div className="flex gap-2">
      {available.map(mod => {
        const checked = selected.includes(mod.id)

        const ids = checked
          ? selected.filter(id => id !== mod.id)
          : [...selected, mod.id]

        const mods = ids.length === 0
          ? 'none'
          : available.length === ids.length
            ? ''
            : ids.join(',')

        return (
          <Link
            key={mod.id}
            className={cn(
              'py-2 px-4 rounded-sm sm:py-1 sm:px-2',
              checked
                ? 'bg-emerald-400 text-white'
                : 'bg-slate-300 text-slate-800',
            )}
            href={{ query: { ...searchParams, mods } }}
          >
            {mod.title}
          </Link>
        )
      })}
    </div>
  )
}
