import Link from 'next/link'
import { findGames } from '@lxndr/gaming-db'
import { BuyMeACoffee } from '~/components/BuyMeACoffee'

export default async function HomePage() {
  const games = await findGames({ visible: true })

  return (
    <>
      <main className="m-4">
        <ul>
          {games.map((game) => (
            <li key={game.id}>
              <Link className="text-blue-500 hover:text-blue-700" href={`/${game.id}`}>
                {game.title}
              </Link>
            </li>
          ))}
        </ul>
      </main>

      <footer className="flex flex-col gap-4 m-4 max-w-xs">
        <BuyMeACoffee />

        <div>
          If you have any questions or suggestions, please use
          <a className="text-blue-500 hover:text-blue-700" target="_blank" rel="noreferrer noopener" href={`email:${process.env.NEXT_PUBLIC_HELPDESK_EMAIL}`}> this&nbsp;email </a>
          or
          <a className="text-blue-500 hover:text-blue-700" target="_blank" rel="noreferrer noopener" href={process.env.NEXT_PUBLIC_ISSUES}> issue&nbsp;tracker</a>
          .
        </div>
      </footer>
    </>
  )
}
