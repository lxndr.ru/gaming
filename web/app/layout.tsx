import { ReactNode } from 'react'
import { Metadata } from 'next'
import { Marmelad } from '@next/font/google'
import { GoogleTagManager } from '@next/third-parties/google'
import './global.css'

const marmelad = Marmelad({
  weight: '400',
  subsets: ['latin'],
  variable: '--font-inter',
})

interface RootLayoutProps {
  children: ReactNode
}

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="en" className={marmelad.variable}>
      <body className="bg-slate-50">{children}</body>
      <GoogleTagManager gtmId={process.env.NEXT_PUBLIC_GA_ID || ''}/>
    </html>
  )
}

export const metadata: Metadata = {
  title: process.env.NEXT_PUBLIC_TITLE,
  description: 'Lists of crafting recipes for various video games',
  keywords: ['crafting recipes', 'crafting components'],
  icons: {
    icon: { url: '/favicon.png', sizes: '512x512' },
    apple: { url: '/favicon.png', sizes: '512x512' },
  },
}
