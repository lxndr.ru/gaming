import { Metadata } from 'next'
import { notFound } from 'next/navigation'
import { findGame, findGames } from '@lxndr/gaming-db'
import { CraftingScreen } from '~/components/CraftingScreen'
import { fetchCraftingData } from '~/components/CraftingScreen/fetchCraftingData'
import type { RawSearchParams } from '~/utils/searchParams'
import { AlchemyScreen } from '~/components/AlchemyScreen'
import { fetchAlchemyData } from '~/components/AlchemyScreen/fetchAlchemyData'

export interface GamePageParams {
  gameId: string
}

export interface GamePageProps {
  params: GamePageParams
  searchParams: RawSearchParams
}

export default async function GamePage({ params, searchParams }: GamePageProps) {
  const game = await findGame(params.gameId)

  if (!game) {
    notFound()
  }

  switch (game.type) {
    case 'alchemy':
      return (
        <AlchemyScreen
          gameData={await fetchAlchemyData(params.gameId)}
          searchParams={searchParams}
        />
      )
    case 'crafting':
      return (
        <CraftingScreen
          gameData={await fetchCraftingData(params.gameId)}
          searchParams={searchParams}
        />
      )
    default:
      notFound()
  }
}

export async function generateStaticParams() {
  const games = await findGames()
  return games.map(({ id, type }) => ({ gameId: id }))
}

export async function generateMetadata({ params }: GamePageProps): Promise<Metadata> {
  const game = await findGame(params.gameId)

  if (!game) {
    notFound()
  }

  return {
    title: `${game.title} | ${process.env.NEXT_PUBLIC_TITLE}`,
    description: game.type === 'crafting'
      ? `${game.title} crafting recipes`
      : `${game.title} alchemy recipes`,
    keywords: game.type === 'crafting'
      ? [`${game.title} crafting`, `${game.title} recipes`]
      : [`${game.title} crafting`, `${game.title} recipes`, `${game.title} alchemy effects`],
  }
}
