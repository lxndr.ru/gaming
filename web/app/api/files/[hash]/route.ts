import { getFile } from '@lxndr/gaming-db'

interface Context {
  params: {
    hash: string
  }
}

export async function GET(req: Request, { params: { hash } }: Context) {
  const file = await getFile(hash)

  if (!file) {
    return new Response('Not found', { status: 404 })
  }

  return new Response(file.data, {
    headers: {
      'Content-Type': file.type,
      'Content-Length': file.size.toString(),
    },
  })
}
