describe('Crafting', () => {
  beforeEach(() => {
    cy.visit('/tes5cr')
  })

  it('should open', () => {
    cy
      .contains('Apple Pie')
      .should('exist')
  })

  it('should filter by category', () => {
    cy
      .get('#filter select')
      .select('baking')
    cy
      .get('#filter #search-btn')
      .click()
    cy
      .url()
      .should('include', 'category=baking')
  })

  it('should search', () => {
    cy
      .get('#filter select')
      .select('baking')
    cy
      .get('#filter input')
      .type('gar')
    cy
      .get('#filter #search-btn')
      .click()
    cy
      .url()
      .should('include', 'search=gar')
    cy
      .get('#item-list tbody tr')
      .should('have.length', 3)
    cy
      .get('#item-list tbody tr:first-child td:first-child')
      .contains('Chicken Dumpling')
      .should('exist')
  })

  it('should clear search', () => {
    cy
      .get('#filter input')
      .type('gar')
    cy
      .get('#filter #clear-btn')
      .click()
    cy
      .url()
      .should('include', '/tes5cr')
  })

  it('should search by clicking item link', () => {
    cy
      .contains('Vegetable Soup')
      .click()
    cy
      .url()
      .should('contain', 'search=Vegetable+Soup')
  })
})
