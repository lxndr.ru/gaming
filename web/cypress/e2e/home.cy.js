describe('Home', () => {
  it('should open home page', () => {
    cy
      .visit('/')
    cy
      .contains('The Elder Scrolls V (Alchemy)')
      .should('exist')
  })

  it('should open game page', () => {
    cy
      .visit('/')
    cy
      .contains('The Elder Scrolls V (Alchemy)')
      .click()
    cy
      .url()
      .should('include', '/tes5al')
  })
})
