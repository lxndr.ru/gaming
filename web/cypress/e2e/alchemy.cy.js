describe('Alchemy', () => {
  beforeEach(() => {
    cy.visit('/tes5al')
  })

  it('should open', () => {
    cy
      .contains('Dragonborn')
      .should('exist')
    cy
      .contains('Briar Heart')
      .should('exist')
  })

  it('should search', () => {
    cy
      .contains('Restore Health')
      .click()
    cy
      .url()
      .should('include', 'effect=restore-health')
    cy
      .contains('Imp Stool')
      .should('exist')
    cy
      .contains('Blisterwort')
      .should('exist')
  })

  it('should switch mods', () => {
    cy
      .visit('/tes5al?effect=damage-health')
    cy
      .contains('Crimson Nirnroot')
      .should('exist')
    cy
      .contains('Emperor Parasol Moss')
      .should('exist')
    cy
      .contains('Poison Bloom')
      .should('exist')
    cy
      .contains('Dawnguard')
      .click()
      .contains('Poison Bloom')
      .should('not.exist')
    cy
      .contains('Dragonborn')
      .click()
      .contains('Emperor Parasol Moss')
      .should('not.exist')
  })
})
