import { Mod } from '@lxndr/gaming-db'

export type RawSearchParamValue = string | string[] | undefined
export type RawSearchParams = Record<string, RawSearchParamValue>

export function parseModsSearchParam(value: RawSearchParamValue, availableMods: Mod[]): string[] {
  const mods = parseStringSearchParam(value)

  if (mods) {
    if (mods === 'none') {
      return ['base']
    }

    return ['base', ...mods.split(',')]
  }

  return availableMods.map(mod => mod.id)
}

export function parseStringSearchParam(value: RawSearchParamValue) {
  return value == null
    ? undefined
    : Array.isArray(value)
      ? value[0]
      : value
}
