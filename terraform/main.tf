terraform {
  backend "http" {
  }

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host      = "tcp://lxndr.ru:2376/"
  cert_path = "${path.cwd}/certs"
}

data "docker_network" "lxndrru" {
  name = "lxndrru"
}
