'use client'

import Image from 'next/image'
import { Item } from '@lxndr/gaming-db'
import { createId } from '@paralleldrive/cuid2'
import { EntityPage } from '~/components/EntityPage'
import { createActionColumn, createStringColumn, createNumberColumn } from '~/components/EntityPage/columns'
import { ItemModal, ItemModelProps } from '~/components/ItemModal'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'item'

interface ItemsPageProps {
  params: {
    gameId: string
    modId: string
  }
}

export default function ItemsPage({ params: { gameId, modId } }: ItemsPageProps) {
  const filter = { gameId, modId }

  const createNewItem = (): Item => ({
    gameId,
    modId,
    id: '',
    title: '',
    description: null,
    iconFileHash: null,
    tag: null,
    price: null,
    weight: null,
    consoleCode: null,
    effects: [],
  })

  const duplicateItem = (item: Item) => ({
    ...item,
    id: '',
    effects: item.effects.map(effect => ({
      ...effect,
      id: createId(),
    })),
  })

  return (
    <EntityPage<Item, ItemModelProps>
      collectionName={COLLECTION_NAME}
      EntityModal={ItemModal}
      entityModalProps={{ gameId }}
      createNewEntity={createNewItem}
      duplicateEntity={duplicateItem}
      getPrimaryKey={({ gameId, modId, id }) => ({ gameId, modId, id })}
      findFilter={filter}
      trpcRouter={trpc.items}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createNumberColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        {
          title: 'Icon',
          dataIndex: 'iconFileHash',
          render: (iconFileHash) => (
            iconFileHash &&
              <Image
                alt="item icon"
                width={24}
                height={24}
                src={`/api/files/${iconFileHash}`}
              />
          ),
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createStringColumn({
          title: 'Description',
          fieldName: 'description',
        }),
        createNumberColumn({
          title: 'Price',
          fieldName: 'price',
        }),
        createNumberColumn({
          title: 'Weight',
          fieldName: 'weight',
        }),
        createStringColumn({
          title: 'Console Code',
          fieldName: 'consoleCode',
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
