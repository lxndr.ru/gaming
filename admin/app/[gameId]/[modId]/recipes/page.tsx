'use client'

import { Recipe } from '@lxndr/gaming-db'
import { createId } from '@paralleldrive/cuid2'
import { EntityPage } from '~/components/EntityPage'
import { RecipeModal, RecipeModelProps } from '~/components/RecipeModal'
import { createActionColumn, createNumberColumn } from '~/components/EntityPage/columns'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'recipe'

interface RecipesPageProps {
  params: {
    gameId: string
    modId: string
  }
}

export default function RecipesPage({ params: { gameId, modId } }: RecipesPageProps) {
  const { data: items } = trpc.items.find.useQuery({ gameId }, { initialData: [] })
  const { data: categories } = trpc.categories.find.useQuery({ gameId }, { initialData: [] })
  const { data: requirements } = trpc.requirements.find.useQuery({ gameId }, { initialData: [] })

  const getItem = (itemId: string) => items.find(({ id }) => id === itemId)

  const getCategory = (categoryId?: string) => categories.find(({ id }) => id === categoryId)

  const getRequirement = (requirementId: string) => requirements.find(({ id }) => id === requirementId)

  const createNewRecipe = (): Recipe => ({
    id: createId(),
    gameId,
    modId,
    itemId: '',
    categoryId: null,
    quantity: 1,
    requirements: [],
    components: [],
  })

  const duplicateRecipe = (recipe: Recipe): Recipe => ({
    ...recipe,
    id: createId(),
    requirements: recipe.requirements.map(requirement => ({
      ...requirement,
      id: createId(),
    })),
    components: recipe.components.map(component => ({
      ...component,
      id: createId(),
    })),
  })

  return (
    <EntityPage<Recipe, RecipeModelProps>
      collectionName={COLLECTION_NAME}
      EntityModal={RecipeModal}
      entityModalProps={{ gameId }}
      createNewEntity={createNewRecipe}
      duplicateEntity={duplicateRecipe}
      getPrimaryKey={({ id }) => ({ id })}
      findFilter={{ gameId, modId }}
      trpcRouter={trpc.recipes}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createNumberColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        {
          title: 'Item',
          dataIndex: 'itemId',
          render: (itemId: string, recipe: Recipe) => {
            const item = getItem(itemId)
            return <>{item?.title} (x{recipe.quantity})</>
          },
          defaultSortOrder: 'ascend',
          sorter: (a, b) => {
            const titleA = getItem(a.itemId)?.title || ''
            const titleB = getItem(b.itemId)?.title || ''
            return titleA.localeCompare(titleB)
          },
        },
        {
          title: 'Category',
          dataIndex: 'categoryId',
          render: (categoryId?: string) => {
            const category = getCategory(categoryId)
            return category?.title
          },
        },
        {
          title: 'Components',
          dataIndex: 'components',
          render: (components: Recipe["components"]) =>
            <ul style={{ margin: 0 }}>
              {components.map(({ id, itemId, quantity }) => {
                const item = getItem(itemId)
                return <li key={id}>{item?.title} (x{quantity})</li>
              })}
            </ul>
        },
        {
          title: 'Requirements',
          dataIndex: 'requirements',
          render: (requirements: Recipe["requirements"]) =>
            <ul style={{ margin: 0 }}>
              {requirements.map(({ id, requirementId, operator, value }) => {
                const requirement = getRequirement(requirementId)
                return requirement
                  ? <li key={id}>{requirement.title} {operator} {value}</li>
                  : <li>loading...</li>
              })}
            </ul>
        },
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
