'use client'

import { Mod } from '@prisma/client'
import Link from 'next/link'
import { EntityPage } from '~/components/EntityPage'
import { createActionColumn, createStringColumn } from '~/components/EntityPage/columns'
import { ModModal } from '~/components/ModModal'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'mod'

interface ModsPageProps {
  params: {
    gameId: string
  }
}

export default function ModsPage({ params: { gameId } }: ModsPageProps) {
  const createNewMod = (): Mod => ({
    id: '',
    gameId,
    title: '',
    abbr: '',
    order: 0,
  })

  const duplicateMod = (mod: Mod) => ({
    ...mod,
    id: '',
    title: '',
    abbr: '',
  })

  return (
    <EntityPage<Mod>
      collectionName={COLLECTION_NAME}
      EntityModal={ModModal}
      createNewEntity={createNewMod}
      duplicateEntity={duplicateMod}
      getPrimaryKey={({ gameId, id }) => ({ gameId, id })}
      trpcRouter={trpc.mods}
      findFilter={{ gameId }}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        {
          title: 'ID',
          dataIndex: 'id',
          sorter: (a, b) => a.id.localeCompare(b.id),
          render: (id: string, mod: Mod) =>
            <Link href={`/${mod.gameId}/${id}/items`}>
              {id}
            </Link>
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
        }),
        createStringColumn({
          title: 'Abbreviation',
          fieldName: 'abbr',
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
