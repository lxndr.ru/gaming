'use client'

import { Requirement } from '@prisma/client'
import { EntityPage } from '~/components/EntityPage'
import { createActionColumn, createStringColumn } from '~/components/EntityPage/columns'
import { RequirementModal } from '~/components/RequirementModal'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'requirement'

interface RequirementsPageProps {
  params: {
    gameId: string
  }
}

export default function RequirementsListPage({ params: { gameId } }: RequirementsPageProps) {
  const createNewRequirement = (): Requirement => ({
    gameId,
    id: '',
    title: '',
  })

  const duplicateRequirement = (category: Requirement) => ({
    ...category,
    id: '',
    title: '',
  })

  return (
    <EntityPage<Requirement>
      collectionName={COLLECTION_NAME}
      EntityModal={RequirementModal}
      createNewEntity={createNewRequirement}
      duplicateEntity={duplicateRequirement}
      getPrimaryKey={({ gameId, id }) => ({ gameId, id })}
      findFilter={{ gameId }}
      trpcRouter={trpc.requirements}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createStringColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
