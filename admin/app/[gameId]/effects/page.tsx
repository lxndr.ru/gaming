'use client'

import { Effect } from '@prisma/client'
import Image from 'next/legacy/image'
import { EntityPage } from '~/components/EntityPage'
import { createActionColumn, createStringColumn } from '~/components/EntityPage/columns'
import { EffectModal } from '~/components/EffectModal'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'effect'

interface EffectsPageProps {
  params: {
    gameId: string
  }
}

export default function EffectsPage({ params: { gameId } }: EffectsPageProps) {
  const createNewEffect = (): Effect => ({
    gameId,
    id: '',
    title: '',
    iconFileHash: null,
  })

  const duplicateEffect = (effect: Effect) => ({
    ...effect,
    id: '',
    title: '',
    iconFileHash: null,
  })

  return (
    <EntityPage<Effect>
      collectionName={COLLECTION_NAME}
      EntityModal={EffectModal}
      createNewEntity={createNewEffect}
      duplicateEntity={duplicateEffect}
      getPrimaryKey={({ gameId, id}) => ({ gameId, id })}
      findFilter={{ gameId }}
      trpcRouter={trpc.effects}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createStringColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        {
          title: 'Icon',
          dataIndex: 'iconFileHash',
          render: (iconFileHash) => (
            iconFileHash &&
              <Image
                alt="effect icon"
                width={24}
                height={24}
                src={`/api/files/${iconFileHash}`}
              />
          ),
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
