'use client'

import { Category } from '@lxndr/gaming-db'
import { EntityPage } from '~/components/EntityPage'
import { createActionColumn, createStringColumn } from '~/components/EntityPage/columns'
import { CategoryModal } from '~/components/CategoryModal'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'category'

interface CategoriesPageProps {
  params: {
    gameId: string
  }
}

export default function CategoriesListPage({ params: { gameId } }: CategoriesPageProps) {
  const createNewCategory = (): Category => ({
    gameId,
    id: '',
    title: '',
  })

  const duplicateCategory = (category: Category) => ({
    ...category,
    id: '',
    title: '',
  })

  return (
    <EntityPage<Category>
      collectionName={COLLECTION_NAME}
      EntityModal={CategoryModal}
      createNewEntity={createNewCategory}
      duplicateEntity={duplicateCategory}
      getPrimaryKey={({ gameId, id }) => ({ gameId, id: id })}
      findFilter={{ gameId }}
      trpcRouter={trpc.categories}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        createStringColumn({
          title: 'ID',
          fieldName: 'id',
        }),
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
