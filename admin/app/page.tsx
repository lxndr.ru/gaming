'use client'

import { Game } from '@prisma/client'
import Link from 'next/link'
import { EntityPage } from '~/components/EntityPage'
import { createActionColumn, createBooleanColumn, createStringColumn } from '~/components/EntityPage/columns'
import { GameModal } from '~/components/GameModal'
import { trpc } from '~/components/trpc'

const COLLECTION_NAME = 'game'

export default function GamesPage() {
  const createNewGame = (): Game => ({
    id: '',
    title: '',
    type: 'crafting',
    version: null,
    visible: false,
  })

  const duplicateGame = (game: Game) => ({
    ...game,
    id: '',
    title: '',
  })

  return (
    <EntityPage<Game>
      collectionName={COLLECTION_NAME}
      EntityModal={GameModal}
      createNewEntity={createNewGame}
      duplicateEntity={duplicateGame}
      getPrimaryKey={({ id }) => ({ id })}
      trpcRouter={trpc.games}
      createColumns={({ onEditClick, onDuplicateClick, onDeleteClick }) => ([
        {
          title: 'ID',
          dataIndex: 'id',
          sorter: (a, b) => a.id.localeCompare(b.id),
          render: (id: string) =>
            <Link href={`/${id}`}>
              {id}
            </Link>
        },
        createStringColumn({
          title: 'Title',
          fieldName: 'title',
          defaultSort: true,
        }),
        createStringColumn({
          title: 'Type',
          fieldName: 'type',
        }),
        createStringColumn({
          title: 'Version',
          fieldName: 'version',
        }),
        createBooleanColumn({
          title: 'Visible',
          fieldName: 'visible',
        }),
        createActionColumn({
          onEditClick,
          onDuplicateClick,
          onDeleteClick,
        }),
      ])}
    />
  )
}
