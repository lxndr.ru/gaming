'use client'

import { ReactNode } from 'react'
import { Layout } from 'antd'
import { TrpcProvider } from 'components/TrpcProvider'
import { MainMenu } from '~/components/MainMenu'
import { ModalProvider } from '~/components/ModalManager'
import './global.css'

const { Header, Content } = Layout

interface RootLayoutProps {
  children: ReactNode
}

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="en">
      <head>
        <title>Gaming Admin</title>
      </head>
      <body>
        <TrpcProvider>
          <ModalProvider>
            <Layout>
              <Header>
                <MainMenu />
              </Header>

              <Content>
                {children}
              </Content>
            </Layout>
          </ModalProvider>
        </TrpcProvider>
      </body>
    </html>
  )
}
