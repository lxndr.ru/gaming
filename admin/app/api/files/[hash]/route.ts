import { getFile } from '@lxndr/gaming-db'

interface GetFileParams {
  hash: string
}

export async function GET(req: Request, { params }: { params: GetFileParams }) {
  const file = await getFile(params.hash)

  if (!file) {
    return new Response('file not found', { status: 404 })
  }

  return new Response(file.data, {
    headers: {
      'Content-Type': file.type,
      'Content-Length': file.size.toString(),
    },
  })
}
