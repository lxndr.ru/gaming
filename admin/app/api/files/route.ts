import { hashBuffer, storeFile } from '@lxndr/gaming-db'

export async function POST(req: Request) {
  const form = await req.formData()
  const uploadedFile = form.get('file') as File | null

  if (!uploadedFile) {
    return new Response('no file provided', { status: 400 })
  }

  const data = Buffer.from(await uploadedFile.arrayBuffer())
  const hash = hashBuffer(data)

  const { data: unused, ...file} = await storeFile({
    type: uploadedFile.type,
    size: uploadedFile.size,
    hash,
    data,
  })

  return Response.json(file)
}
