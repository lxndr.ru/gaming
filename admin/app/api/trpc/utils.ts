import * as z from 'zod'

export const StringIdSchema = z.string().trim().min(1)
