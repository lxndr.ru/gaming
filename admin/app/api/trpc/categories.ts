import * as z from 'zod'
import { findCategories, upsertCategory, deleteCategory } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const categoriesRouter = router({
  find: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
    }))
    .query(({ input }) => findCategories(input.gameId)),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
      }),
      data: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
        title: z.string().trim().min(1),
      }),
    }))
    .mutation(({ input }) => upsertCategory(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      id: StringIdSchema,
    }))
    .mutation(({ input }) => deleteCategory(input)),
})
