import * as z from 'zod'
import { findItems, upsertItem, deleteItem } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const itemsRouter = router({
  find: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      modId: StringIdSchema.optional(),
    }))
    .query(({ input }) => findItems(input)),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        gameId: StringIdSchema,
        modId: StringIdSchema,
        id: StringIdSchema,
      }),
      data: z.object({
        gameId: StringIdSchema,
        modId: StringIdSchema,
        id: StringIdSchema,
        title: z.string().trim().min(1),
        description: z.string().trim().min(1).nullable(),
        iconFileHash: z.string().min(64).max(64).nullable(),
        tag: z.string().trim().toLowerCase().min(1).nullable(),
        price: z.number().nullable(),
        weight: z.number().nullable(),
        consoleCode: z.string().trim().min(1).nullable(),
        effects: z.array(
          z.object({
            id: StringIdSchema,
            effectId: StringIdSchema,
            magnitude: z.string().trim().min(1).nullable(),
            duration: z.number().nullable(),
          }),
        ),
      }),
    }))
    .mutation(({ input }) => upsertItem(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      modId: StringIdSchema,
      id: StringIdSchema,
    }))
    .mutation( ({ input }) => deleteItem(input)),
})
