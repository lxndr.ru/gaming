import * as z from 'zod'
import { deleteMod, findMods, upsertMod } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const modsRouter = router({
  find: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
    }))
    .query(({ input }) => findMods(input.gameId)),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
      }),
      data: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
        title: z.string().trim(),
        abbr: z.string().trim().toUpperCase(),
        order: z.number().int(),
      }),
    }))
    .mutation(({ input }) => upsertMod(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      id: StringIdSchema,
    }))
    .mutation(({ input }) => deleteMod(input)),
})
