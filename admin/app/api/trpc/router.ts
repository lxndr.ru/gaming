import { router } from './trpc'
import { gamesRouter } from './games'
import { modsRouter } from './mods'
import { recipesRouter } from './recipes'
import { effectsRouter } from './effects'
import { categoriesRouter } from './categories'
import { requirementsRouter } from './requirements'
import { itemsRouter } from './items'

export const appRouter = router({
  games: gamesRouter,
  mods: modsRouter,
  effects: effectsRouter,
  categories: categoriesRouter,
  requirements: requirementsRouter,
  items: itemsRouter,
  recipes: recipesRouter,
})

export type AppRouter = typeof appRouter
