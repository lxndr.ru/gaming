import * as z from 'zod'
import { deleteRecipe, findRecipes, upsertRecipe } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const recipesRouter = router({
  find: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      modId: StringIdSchema,
    }))
    .query(({ input }) => findRecipes(input.gameId, input.modId)),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        id: StringIdSchema,
      }),
      data: z.object({
        gameId: StringIdSchema,
        modId: StringIdSchema,
        id: StringIdSchema,
        itemId: StringIdSchema,
        categoryId: StringIdSchema.nullable(),
        quantity: z.number(),
        requirements: z.array(
          z.object({
            id: StringIdSchema,
            requirementId: StringIdSchema,
            operator: z.string().min(1).nullable(),
            value: z.string().min(1).nullable(),
          }),
        ),
        components: z.array(
          z.object({
            id: StringIdSchema,
            itemId: StringIdSchema,
            quantity: z.number(),
          }),
        ),
      }),
    }))
    .mutation(({ input }) => upsertRecipe(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      id: z.string().trim().min(1),
    }))
    .mutation(({ input }) => deleteRecipe(input)),
})
