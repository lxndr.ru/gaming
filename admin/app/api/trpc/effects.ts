import * as z from 'zod'
import { findEffects, upsertEffect, deleteEffect } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const effectsRouter = router({
  find: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
    }))
    .query(({ input }) => findEffects(input.gameId)),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
      }),
      data: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
        title: z.string().trim().min(1),
        iconFileHash: z.string().min(64).max(64).nullable(),
      }),
    }))
    .mutation( ({ input }) => upsertEffect(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      id: StringIdSchema,
    }))
    .mutation( ({ input }) => deleteEffect(input)),
})
