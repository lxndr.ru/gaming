import * as z from 'zod'
import { deleteGame, findGames, upsertGame } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const gamesRouter = router({
  find: publicProcedure
    .input(z.object({}))
    .query(() => findGames()),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        id: StringIdSchema,
      }),
      data: z.object({
        id: StringIdSchema,
        title: z.string().trim().min(1),
        type: z.string().trim(),
        version: z.string().trim().min(1).nullable(),
        visible: z.boolean(),
      }),
    }))
    .mutation(({ input }) => upsertGame(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      id: StringIdSchema,
    }))
    .mutation(({ input }) => deleteGame(input.id)),
})
