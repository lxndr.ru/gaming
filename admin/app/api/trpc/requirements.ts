import * as z from 'zod'
import { findRequirements, upsertRequirement, deleteRequirement } from '@lxndr/gaming-db'
import { publicProcedure, router } from './trpc'
import { StringIdSchema } from './utils'

export const requirementsRouter = router({
  find: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
    }))
    .query(({ input }) => findRequirements(input.gameId)),

  upsert: publicProcedure
    .input(z.object({
      key: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
      }),
      data: z.object({
        gameId: StringIdSchema,
        id: StringIdSchema,
        title: z.string().trim().min(1),
      }),
    }))
    .mutation( ({ input }) => upsertRequirement(input.key, input.data)),

  delete: publicProcedure
    .input(z.object({
      gameId: StringIdSchema,
      id: StringIdSchema,
    }))
    .mutation( ({ input }) => deleteRequirement(input)),
})
