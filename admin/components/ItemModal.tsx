import { FC } from 'react'
import { createId } from '@paralleldrive/cuid2'
import { Modal, Form, Input, InputNumber, Select, Typography, Button, Space } from 'antd'
import { CaretDownOutlined, CaretUpOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'
import { Item } from '@lxndr/gaming-db'
import { EntityModalProps } from '~/components/EntityPage'
import { GenericModal } from '~/components/ModalManager'
import { IconInput } from '~/components/IconInput'
import { IDInput } from '~/components/IDInput'
import { trpc } from './trpc'

export interface ItemModelProps extends GenericModal, EntityModalProps<Item> {
  gameId: string
}

const tagOptions = [
  {
    value: 'quest',
    label: 'Quest item',
  },
  {
    value: 'uniqe',
    label: 'Unique item',
  }
]

export const ItemModal: FC<ItemModelProps> = ({
  gameId,
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Item>()
  const title = Form.useWatch('title', form)
  const { data: effectOptions } = trpc.effects.find.useQuery({ gameId }, {
    select: (effects => effects.map(effect => ({
      value: effect.id,
      label: effect.title,
    }))),
  })

  const handleSubmit = async () => {
    const item = await form.validateFields()

    const normalized: Item = {
      ...item,
      description: item.description || null,
      price: item.price ?? null,
      weight: item.weight ?? null,
      tag: item.tag || null,
      consoleCode: item.consoleCode || null,
      effects: item.effects.map(effect => ({
        ...effect,
        magnitude: effect.magnitude ?? null,
        duration: effect.duration ?? null,
      }))
    }

    await onSubmit(normalized)
  }

  return (
    <Modal open title="Item" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        size="small"
        labelCol={{ span: 6, }}
        wrapperCol={{ span: 18 }}
      >
        <Form.Item
          hidden
          name="gameId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          hidden
          name="modId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="description"
          label="Description"
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          name="iconFileHash"
          label="Icon"
        >
          <IconInput />
        </Form.Item>

        <Form.Item
          name="price"
          label="Price"
        >
          <InputNumber  />
        </Form.Item>

        <Form.Item
          name="weight"
          label="Weight"
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          name="tag"
          label="Tag"
        >
          <Select
            allowClear
            options={tagOptions}
          />
        </Form.Item>

        <Form.Item
          name="consoleCode"
          label="Console Code"
        >
          <Input />
        </Form.Item>

        <Typography.Title level={5}>Effects</Typography.Title>

        <Form.List name="effects">
          {(fields, { add, remove, move }) => (
            <>
              {fields.map(({ key, name }, index) => (
                <div key={key} style={{ display: 'flex', gap: '8px', marginBottom: '16px' }}>
                  <Form.Item hidden name={[name, 'id']}>
                    <Input />
                  </Form.Item>
                  <Form.Item
                    noStyle
                    name={[name, 'effectId']}
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="Effect"
                      optionFilterProp="label"
                      options={effectOptions}
                      style={{ flexGrow: 1 }}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Mag"
                    colon={false}
                    name={[name, 'magnitude']}
                    style={{ marginBottom: 0, flexGrow: 1 }}
                  >
                    <Input />
                  </Form.Item>

                  <Space.Compact>
                    <Button disabled={index === 0} icon={<CaretUpOutlined />} onClick={() => move(index, index - 1)} />
                    <Button disabled={index >= fields.length - 1} icon={<CaretDownOutlined />} onClick={() => move(index, index + 1)} />
                    <Button danger type="primary" icon={<MinusCircleOutlined />} onClick={() => remove(name)} />
                  </Space.Compact>
                </div>
              ))}

              <Button
                type="dashed"
                style={{ width: '200px' }}
                icon={<PlusOutlined />}
                onClick={() => add({
                  id: createId(),
                })}
              >
                Add effect
              </Button>
            </>
          )}
        </Form.List>
      </Form>
    </Modal>
  )
}
