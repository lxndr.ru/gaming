export type GenericEntity = {
    id?: string
    [x: string]: unknown
}
