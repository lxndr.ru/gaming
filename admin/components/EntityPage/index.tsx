import { FC, useEffect } from 'react'
import { notification, Button, Table, Modal } from 'antd'
import { ColumnsType } from 'antd/lib/table'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { useModal } from '~/components/ModalManager'
import { CreateActionColumn } from './columns'
import { GenericEntity } from './type'

export interface EntityModalProps<T extends GenericEntity> {
  initialValues: T
  onSubmit: (updatedEntity: T) => Promise<void>
}

interface EntityPageProps<
  ENTITY extends GenericEntity,
  MODAL_PROPS extends EntityModalProps<ENTITY> = EntityModalProps<ENTITY>,
> {
  collectionName: string
  EntityModal: FC<MODAL_PROPS>
  entityModalProps?: Partial<MODAL_PROPS>
  createColumns: (options: CreateActionColumn<ENTITY>) => ColumnsType<ENTITY>
  createNewEntity: () => ENTITY
  duplicateEntity: (entity: ENTITY) => ENTITY
  getPrimaryKey: (entity: ENTITY) => Partial<ENTITY>
  trpcRouter: any
  findFilter?: Record<string, unknown>
}

const handleApiRequestError = (err: unknown) => {
  const description =
    err instanceof Error
      ? err.message
      : 'Error!'

  notification.error({
    message: 'Error',
    description: (
      <pre style={{ fontSize: 'small' }}>
        {description}
      </pre>
    ),
  })
}

export function EntityPage<
  ENTITY extends GenericEntity,
  MODAL_PROPS extends EntityModalProps<ENTITY> = EntityModalProps<ENTITY>,
>({
  collectionName,
  EntityModal,
  entityModalProps = {},
  createColumns,
  createNewEntity,
  duplicateEntity,
  getPrimaryKey,
  trpcRouter,
  findFilter = {},
}: EntityPageProps<ENTITY, MODAL_PROPS>) {
  const { openModal } = useModal()

  let findQuery = trpcRouter.find.useQuery(findFilter, {
    initialData: [],
  })

  useEffect(() => {
    if (findQuery.error) {
      handleApiRequestError(findQuery.error)
    }
  }, [findQuery.error])

  const upsertMutation = trpcRouter.upsert.useMutation({
    onError: handleApiRequestError,
  })

  const deleteMutation = trpcRouter.delete.useMutation({
    onError: handleApiRequestError,
  })

  const openEntityModal = (entity: ENTITY) => {
    // @ts-ignore
    const closeModal = openModal(EntityModal, {
      ...entityModalProps,
      initialValues: entity,
      onSubmit: async (updatedEntity) => {
        await upsertMutation.mutateAsync({
          key: {
            ...getPrimaryKey(entity),
            id: entity.id || updatedEntity.id,
          },
          data: updatedEntity,
        })
        await findQuery.refetch()
        closeModal()
      },
    })
  }

  const handleAddEntity = () =>
    openEntityModal(createNewEntity())

  const handleEditEntity = (entity: ENTITY) =>
    openEntityModal(entity)

  const handleDuplicateEntity = (entity: ENTITY) =>
    openEntityModal(duplicateEntity(entity))

  const handleDeleteEntity = (entity: ENTITY) => {
    Modal.confirm({
      title: 'Confirmation',
      icon: <ExclamationCircleOutlined />,
      content: 'Are you sure you want to delete this item?',
      onOk: async () => {
        await deleteMutation.mutateAsync(entity)
        await findQuery.refetch()
      },
    })
  }

  return (
    <>
      <Button
        type="primary"
        style={{ margin: '16px'}}
        onClick={handleAddEntity}
      >
        Add new {collectionName}
      </Button>

      <Table<ENTITY>
        dataSource={findQuery.data}
        rowKey={entity => JSON.stringify(getPrimaryKey(entity))}
        pagination={false}
        size="small"
        showSorterTooltip={false}
        loading={findQuery.isFetching}
        columns={createColumns({
          onEditClick: handleEditEntity,
          onDuplicateClick: handleDuplicateEntity,
          onDeleteClick: handleDeleteEntity,
        })}
      />
    </>
  )
}
