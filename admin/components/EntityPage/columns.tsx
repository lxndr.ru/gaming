import { CopyOutlined, DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Space } from 'antd'
import { ColumnType } from 'antd/lib/table'

import { GenericEntity } from './type'

interface CreateSortingColumnOptions {
  title: string
  fieldName: string
  defaultSort?: boolean
}

export const createStringColumn = <T extends GenericEntity>({
  title,
  fieldName,
  defaultSort,
}: CreateSortingColumnOptions): ColumnType<T> => ({
  title,
  dataIndex: fieldName,
  defaultSortOrder: defaultSort ? 'ascend' : undefined,
  sorter: (a: any, b: any) => a[fieldName].localeCompare(b[fieldName]),
})

export const createNumberColumn = <T extends GenericEntity>({
  title,
  fieldName,
  defaultSort,
}: CreateSortingColumnOptions): ColumnType<T> => ({
  title,
  dataIndex: fieldName,
  defaultSortOrder: defaultSort ? 'ascend' : undefined,
  sorter: (a: any, b: any) => a[fieldName] - b[fieldName],
})

export const createBooleanColumn = <T extends GenericEntity>({
  title,
  fieldName,
  defaultSort,
}: CreateSortingColumnOptions): ColumnType<T> => ({
  title,
  dataIndex: fieldName,
  defaultSortOrder: defaultSort ? 'ascend' : undefined,
  sorter: (a: any, b: any) => a[fieldName] - b[fieldName],
  render: (value: boolean) =>
    <>
      {value ? 'Yes' : 'No'}
    </>
})

export interface CreateActionColumn<ENTITY extends GenericEntity> {
  onEditClick: (entity: ENTITY) => void
  onDuplicateClick: (entity: ENTITY) => void
  onDeleteClick: (entity: ENTITY) => void
}

export const createActionColumn = <T extends GenericEntity>({
  onEditClick,
  onDuplicateClick,
  onDeleteClick,
}: CreateActionColumn<T>): ColumnType<T> => ({
  key: 'actions',
  width: '100px',
  render: (value: unknown, entity: T) =>
    <Space.Compact>
      <Button size="small" icon={<EditOutlined />} title="Edit" onClick={() => onEditClick(entity)} />
      <Button size="small" icon={<CopyOutlined />} title="Duplicate" onClick={() => onDuplicateClick(entity)} />
      <Button danger size="small" icon={<DeleteOutlined />} title="Delete" onClick={() => onDeleteClick(entity)} />
    </Space.Compact>
})
