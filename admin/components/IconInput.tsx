import { FC, useState } from 'react'
import { Upload } from 'antd'
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface'
import { LoadingOutlined, UploadOutlined } from '@ant-design/icons'

interface IconInputProps {
  value?: string
  onChange?: (value: string) => void
}

export const IconInput: FC<IconInputProps> = ({ value: iconFileHash, onChange }) => {
  const [loading, setLoading] = useState(false);

  const handleUpload = ({ file }: UploadChangeParam<UploadFile<{ hash: string }>>) => {
    switch (file.status) {
    case 'uploading':
      setLoading(true)
      break
    case 'done':
      if (file.response) {
        onChange?.(file.response.hash)
      }

      setLoading(false)
      break
    }
  }

  return (
    <Upload
      listType="picture-card"
      showUploadList={false}
      action="/api/files"
      onChange={handleUpload}
    >
      {iconFileHash ? (
        <img
          alt="avatar"
          src={`/api/files/${iconFileHash}`}
          style={{ width: '100%' }}
        />
      ) : (
        <div>
          {loading ? <LoadingOutlined /> : <UploadOutlined />}
          <div style={{ marginTop: 8 }}>Upload</div>
        </div>
      )}
    </Upload>
  )
}
