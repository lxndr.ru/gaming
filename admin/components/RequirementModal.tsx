import { FC } from 'react'
import { Requirement } from '@prisma/client'
import { Modal, Form, Input } from 'antd'
import { GenericModal } from '~/components/ModalManager'
import { IDInput } from '~/components/IDInput'
import { EntityModalProps } from '~/components/EntityPage'

interface RequirementModalProps extends GenericModal, EntityModalProps<Requirement> {}

export const RequirementModal: FC<RequirementModalProps> = ({
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Requirement>()
  const title = Form.useWatch('title', form)

  const handleSubmit = async () => {
    const category = await form.validateFields()
    await onSubmit(category)
  }

  return (
    <Modal open title="Requirement" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <Form.Item
          hidden
          name="gameId"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item required name="id" label="ID" htmlFor="id" rules={[{ required: true }]}>
          <IDInput id="id" idSource={title} />
        </Form.Item>

        <Form.Item
          name="title"
          label="Title"
          rules={[
            { required: true },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  )
}
