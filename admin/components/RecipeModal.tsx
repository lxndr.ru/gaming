import { FC } from 'react'
import { createId } from '@paralleldrive/cuid2'
import { Modal, Form, Input, InputNumber, Select, Typography, Button, Space } from 'antd'
import { CaretDownOutlined, CaretUpOutlined, MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'
import { Recipe } from '@lxndr/gaming-db'
import { EntityModalProps } from '~/components/EntityPage'
import { GenericModal } from '~/components/ModalManager'
import { trpc } from './trpc'

export interface RecipeModelProps extends GenericModal, EntityModalProps<Recipe> {
  gameId: string
  modId: string
}

export const RecipeModal: FC<RecipeModelProps> = ({
  gameId,
  initialValues,
  onCancel,
  onSubmit,
}) => {
  const [form] = Form.useForm<Recipe>()

  const { data: itemOptions } = trpc.items.find.useQuery({ gameId }, {
    select(items) {
      return items.map(item => ({
        value: item.id,
        label: item.title,
      }))
    },
  })

  const { data: categoryOptions } = trpc.categories.find.useQuery({ gameId }, {
    select(categories) {
      return categories.map(category => ({
        value: category.id,
        label: category.title,
      }))
    },
  })

  const { data: requirementOptions } = trpc.requirements.find.useQuery({ gameId }, {
    select(requirements) {
      return requirements.map(requirement => ({
        value: requirement.id,
        label: requirement.title,
      }))
    }
  })

  const handleSubmit = async () => {
    const recipe = await form.validateFields()

    const normalizedRecipe: Recipe = {
      ...recipe,
      categoryId: recipe.categoryId || null,
      requirements: recipe.requirements.map(requirement => ({
        ...requirement,
        operator: requirement.operator || null,
        value: requirement.value || null,
      })),
      components: recipe.components.map(component => ({
        ...component,
        quantity: component.quantity || null,
      })),
    }
  
    await onSubmit(normalizedRecipe)
  }

  return (
    <Modal open title="Recipe" onCancel={onCancel} onOk={handleSubmit}>
      <Form
        form={form}
        initialValues={initialValues}
        size="small"
        labelCol={{ span: 6, }}
        wrapperCol={{ span: 18 }}
      >
        <Form.Item hidden name="id" />
        <Form.Item hidden name="gameId" />
        <Form.Item hidden name="modId" />

        <Form.Item
          name="itemId"
          label="Item"
          rules={[{ required: true }]}
        >
          <Select
            showSearch
            placeholder="Item"
            optionFilterProp="label"
            options={itemOptions}
          />
        </Form.Item>

        <Form.Item
          name="quantity"
          label="Qty"
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          name="categoryId"
          label="Category"
        >
          <Select allowClear options={categoryOptions} />
        </Form.Item>

        <Typography.Title level={5}>Components</Typography.Title>

        <Form.List name="components">
          {(fields, { add, remove, move }) =>
            <>
              {fields.map(({ key, name }, index) =>
                <div key={key} style={{ display: 'flex', gap: '8px', marginBottom: '16px' }}>
                  <Form.Item hidden name={[name, 'id']} />
                  <Form.Item
                    noStyle
                    name={[name, 'itemId']}
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="Item"
                      optionFilterProp="label"
                      options={itemOptions}
                      style={{ flexGrow: 1 }}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Qty"
                    name={[name, 'quantity']}
                    style={{ marginBottom: 0 }}
                  >
                    <InputNumber />
                  </Form.Item>

                  <Space.Compact>
                    <Button disabled={index === 0} icon={<CaretUpOutlined />} onClick={() => move(index, index - 1)} />
                    <Button disabled={index >= fields.length - 1} icon={<CaretDownOutlined />} onClick={() => move(index, index + 1)} />
                    <Button danger type="primary" icon={<MinusCircleOutlined />} onClick={() => remove(name)} />
                  </Space.Compact>
                </div>
              )}

              <Button
                type="dashed"
                style={{ width: '200px' }}
                icon={<PlusOutlined />}
                onClick={() => add({
                  id: createId(),
                  quantity: 1,
                })}
              >
                Add component
              </Button>
            </>
          }
        </Form.List>

        <Typography.Title level={5}>Requirements</Typography.Title>

        <Form.List name="requirements">
          {(fields, { add, remove, move }) =>
            <>
              {fields.map(({ key, name }, index) => (
                <div key={key} style={{ display: 'flex', gap: '8px', marginBottom: '16px' }}>
                  <Form.Item hidden name={[name, 'id']} />
                  <Form.Item
                    noStyle
                    name={[name, 'requirementId']}
                    rules={[{ required: true }]}
                  >
                    <Select
                      showSearch
                      placeholder="Requirement"
                      optionFilterProp="label"
                      options={requirementOptions}
                      style={{ flexGrow: 1 }}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Op"
                    name={[name, 'operator']}
                    style={{ marginBottom: 0, width: '80px' }}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    label="Val"
                    name={[name, 'value']}
                    style={{ marginBottom: 0, width: '80px' }}
                  >
                    <Input />
                  </Form.Item>

                  <Space.Compact>
                    <Button disabled={index === 0} icon={<CaretUpOutlined />} onClick={() => move(index, index - 1)} />
                    <Button disabled={index >= fields.length - 1} icon={<CaretDownOutlined />} onClick={() => move(index, index + 1)} />
                    <Button danger type="primary" icon={<MinusCircleOutlined />} onClick={() => remove(name)} />
                  </Space.Compact>
                </div>
              ))}

              <Button
                type="dashed"
                style={{ width: '200px' }}
                icon={<PlusOutlined />}
                onClick={() => add({
                  id: createId(),
                })}
              >
                Add requirement
              </Button>
            </>
          }
        </Form.List>
      </Form>
    </Modal>
  )
}
