import Link from 'next/link'
import { useParams, usePathname } from 'next/navigation'
import { Menu, MenuProps } from 'antd'

export const MainMenu = () => {
  const pathname = usePathname() || '/'
  const { gameId, modId } = useParams<{ gameId: string; modId: string }>()

  const menuItems: MenuProps['items'] = [
    {
      key: '/',
      label: (
        <Link href="/">
          Games
        </Link>
      ),
    },
  ]

  if (gameId) {
    const path = `/${gameId}`

    menuItems.push({
      key: path,
      label: (
        <Link href={path}>
          Mods
        </Link>
      ),
    })

    const effectsPath = `/${gameId}/effects`

    menuItems.push({
      key: effectsPath,
      label: (
        <Link href={effectsPath}>
          Effects
        </Link>
      ),
    })

    const categoriesPath = `/${gameId}/categories`

    menuItems.push({
      key: categoriesPath,
      label: (
        <Link href={categoriesPath}>
          Categories
        </Link>
      ),
    })

    const requirementsPath = `/${gameId}/requirements`

    menuItems.push({
      key: requirementsPath,
      label: (
        <Link href={requirementsPath}>
          Requirements
        </Link>
      ),
    })
  }

  if (modId) {
    const itemsPath = `/${gameId}/${modId}/items`

    menuItems.push({
      key: itemsPath,
      label: (
        <Link href={itemsPath}>
          Items
        </Link>
      ),
    })
  
    const recipesPath = `/${gameId}/${modId}/recipes`

    menuItems.push({
      key: recipesPath,
      label: (
        <Link href={recipesPath}>
          Recepies
        </Link>
      ),
    })
  }

  return (
    <Menu
      theme="dark"
      mode="horizontal"
      items={menuItems}
      selectedKeys={[pathname]}
    />
  )
}
