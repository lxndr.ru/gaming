import React, {
  FC,
  createContext,
  useContext,
  useMemo,
  useState,
  ReactNode,
  useRef,
  useCallback,
} from 'react'

export interface GenericModal {
  onCancel?: () => void
}

interface ModalParams<T extends GenericModal> {
  id: number
  Component: FC<T>
  props: T
}

interface ModalContextValue {
  openModal: <T>(Component: FC<T>, props: T) => () => void
}

interface ModalProviderProps {
  children: ReactNode
}

const ModalContext = createContext<ModalContextValue>({
  openModal: () => {
    console.error('You might have forgotten to wrap you app in ModalProvider')
    return () => {}
  },
})

export const ModalProvider: FC<ModalProviderProps> = ({ children }) => {
  const refID = useRef(0)
  const [modals, setModals] = useState<ModalParams<any>[]>([])

  const nextID = () => refID.current++

  const openModal = useCallback((Component: FC<any>, props: any) => {
    const id = nextID()

    const closeModal = () =>
      setModals((prevModals) => prevModals.filter((modal) => modal.id !== id))

    const newModal = {
      id,
      Component,
      props: {
        onCancel: closeModal,
        ...props
      }
    }

    setModals((prevModals) => {
      const newModals = prevModals.slice()
      newModals.push(newModal)
      return newModals
    })

    return closeModal
  }, []);

  const value = useMemo(() => ({
    openModal,
  }), [openModal])

  return (
    <ModalContext.Provider value={value}>
      {children}
      {modals.map(({ id, Component, props }) =>
        <Component key={id} {...props} />)}
    </ModalContext.Provider>
  )
}

export const useModal = () => useContext(ModalContext)
