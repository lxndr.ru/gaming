import { Game } from '@prisma/client'
import { prisma } from './client'

export { Game }
export type GameKey = Pick<Game, 'id'>

export interface FindGamesOptions {
  visible?: boolean
}

export async function findGames(options?: FindGamesOptions): Promise<Game[]> {
  return await prisma.game.findMany({
    where: options,
    orderBy: {
      title: 'asc',
    },
  })
}

export async function findGame(id: string) {
  return await prisma.game.findUnique({
    where: { id },
  })
}

export async function upsertGame(key: GameKey, game: Game) {
  await prisma.game.upsert({
    where: { id: key.id },
    create: game,
    update: game,
    select: null,
  })
}

export async function deleteGame(id: string) {
  await prisma.game.delete({
    where: { id },
    select: null,
  })
}
