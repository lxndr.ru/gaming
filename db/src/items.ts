import { Item as PrismaItem, ItemEffect as PrismaItemEffect } from '@prisma/client'
import { prisma } from './client'

type ItemEffect = Omit<PrismaItemEffect, 'gameId' | 'modId' | 'itemId' | 'order'>

export type Item = PrismaItem & {
  effects: ItemEffect[]
}

export type ItemKey = Pick<Item, 'gameId' | 'modId' | 'id'>

interface FindItemsOptions {
  gameId: string
  modId?: string
}

export async function findItems(filter: FindItemsOptions): Promise<Item[]> {
  return await prisma.item.findMany({
    where: filter,
    include: {
      effects: {
        orderBy: {
          order: 'asc',
        },
        select: {
          id: true,
          effectId: true,
          magnitude: true,
          duration: true,
        },
      },
    },
  })
}

export async function upsertItem(key: ItemKey, item: Item) {
  await prisma.$transaction(async tx => {
    let effectsToDelete: string[] = []

    const existingItem = await tx.item.findUnique({
      where: { gameId_modId_id: key },
      include: {
        effects: true,
      },
    })

    if (existingItem) {
      for (const { id } of existingItem.effects) {
        if (!item.effects.find(it => it.id === id)) {
          effectsToDelete.push(id)
        }
      }
    }

    const effects = item.effects.map((effect, index) => ({
      ...effect,
      order: index,
    }))

    const baseItem = {
      gameId: item.gameId,
      id: item.id,
    }

    await tx.baseItem.upsert({
      where: {
        gameId_id: { gameId: key.gameId, id: key.id },
      },
      create: baseItem,
      update: baseItem,
      select: null,
    })

    await tx.item.upsert({
      where: {
        gameId_modId_id: { ...baseItem, modId: key.modId },
      },
      create: {
        ...item,
        effects: {
          createMany: {
            data: effects,
          },
        },
      },
      update: {
        ...item,
        effects: {
          upsert: effects.map(effect => ({
            where: { id: effect.id },
            create: effect,
            update: effect,
          })),
          deleteMany: effectsToDelete.map(id => ({ id })),
        },
      },
      select: null,
    })
  })
}

export async function deleteItem(key: ItemKey) {
  await prisma.$transaction(async tx => {
    await tx.item.delete({
      where: { gameId_modId_id: key },
      select: null,
    })

    await tx.baseItem.deleteMany({
      where: {
        items: {
          none: {},
        },
      },
    })
  })
}
