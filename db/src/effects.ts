import { Effect } from '@prisma/client'
import { prisma } from './client'

export { Effect }
export type EffectKey = Pick<Effect, 'gameId' | 'id'>

export async function findEffects(gameId: string): Promise<Effect[]> {
  return await prisma.effect.findMany({
    where: { gameId },
  })
}

export async function upsertEffect(key: EffectKey, effect: Effect) {
  await prisma.effect.upsert({
    where: { gameId_id: key },
    create: effect,
    update: effect,
    select: null,
  })
}

export async function deleteEffect(key: EffectKey) {
  await prisma.effect.delete({
    where: { gameId_id: key },
    select: null,
  })
}
