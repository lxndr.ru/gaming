import {
  Recipe as PrismaRecipe,
  RecipeRequirement as PrismaRecipeRequirement,
  RecipeComponent as PrismaRecipeComponent,
} from '@prisma/client'
import { prisma } from './client'

type RecipeRequirement = Omit<PrismaRecipeRequirement, 'gameId' | 'recipeId' | 'order'>
type RecipeComponent = Omit<PrismaRecipeComponent, 'gameId' | 'recipeId' | 'order'>

export type Recipe = PrismaRecipe & {
  components: RecipeComponent[]
  requirements: RecipeRequirement[]
}

export type RecipeKey = Pick<Recipe, 'id'>

export async function findRecipes(gameId: string, modId: string): Promise<Recipe[]> {
  const recipes = await prisma.recipe.findMany({
    where: {
      gameId,
      modId,
    },
    include: {
      components: {
        orderBy: {
          order: 'asc',
        },
        select: {
          id: true,
          itemId: true,
          quantity: true,
        },
      },
      requirements: {
        orderBy: {
          order: 'asc',
        },
        select: {
          id: true,
          requirementId: true,
          operator: true,
          value: true,
        },
      },
    },
  })

  return recipes.map(recipe => ({
    ...recipe,
    id: String(recipe.id),
  }))
}

export async function upsertRecipe(key: RecipeKey, recipe: Recipe) {
  let requirementsToDelete: string[] = []
  let componentsToDelete: string[] = []

  const existingRecipe = await prisma.recipe.findUnique({
    where: key,
    include: {
      requirements: true,
      components: true,
    },
  })

  if (existingRecipe) {
    for (const { id } of existingRecipe.requirements) {
      if (!recipe.requirements.find(it => it.id === id)) {
        requirementsToDelete.push(id)
      }
    }

    for (const { id } of existingRecipe.components) {
      if (!recipe.components.find(it => it.id === id)) {
        componentsToDelete.push(id)
      }
    }
  }

  const requirements = recipe.requirements.map(requirement => ({
    ...requirement,
    gameId: recipe.gameId,
  }))

  const components = recipe.components.map((component, index) => ({
    ...component,
    order: index,
    gameId: recipe.gameId,
  }))

  await prisma.recipe.upsert({
    where: key,
    create: {
      ...recipe,
      requirements: {
        createMany: { data: requirements },
      },
      components: {
        createMany: { data: components },
      },
    },
    update: {
      ...recipe,
      requirements: {
        upsert: requirements.map(requirement => ({
          where: { id: requirement.id },
          create: requirement,
          update: requirement,
        })),
        deleteMany: requirementsToDelete.map(id => ({ id })),
      },
      components: {
        upsert: components.map(component => ({
          where: { id: component.id },
          create: component,
          update: component,
        })),
        deleteMany: componentsToDelete.map(id => ({ id })),
      },
    },
    select: null,
  })
}

export async function deleteRecipe(key: RecipeKey) {
  await prisma.recipe.delete({
    where: key,
    select: null,
  })
}
