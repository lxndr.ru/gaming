import { Mod } from '@prisma/client'
import { prisma } from './client'

export { Mod }
export type ModKey = Pick<Mod, 'gameId' | 'id'>

export async function findMods(gameId: string): Promise<Mod[]> {
  return await prisma.mod.findMany({
    where: { gameId },
    orderBy: {
      order: 'asc',
    },
  })
}

export async function upsertMod(key: ModKey, category: Mod) {
  await prisma.mod.upsert({
    where: { gameId_id: key },
    create: category,
    update: category,
    select: null,
  })
}

export async function deleteMod(key: ModKey) {
  await prisma.mod.delete({
    where: { gameId_id: key },
    select: null,
  })
}
