import { Category } from '@prisma/client'
import { prisma } from './client'

export { Category }
export type CategoryKey = Pick<Category, 'gameId' | 'id'>

export async function findCategories(gameId: string): Promise<Category[]> {
  return await prisma.category.findMany({
    where: { gameId },
  })
}

export async function upsertCategory(key: CategoryKey, category: Category) {
  await prisma.category.upsert({
    where: { gameId_id: key },
    create: category,
    update: category,
    select: null,
  })
}

export async function deleteCategory(key: CategoryKey) {
  await prisma.category.delete({
    where: { gameId_id: key },
    select: null,
  })
}
