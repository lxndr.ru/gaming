import { Requirement } from '@prisma/client'
import { prisma } from './client'

export { Requirement }
export type RequirementKey = Pick<Requirement, 'gameId' | 'id'>

export async function findRequirements(gameId: string): Promise<Requirement[]> {
  return await prisma.requirement.findMany({
    where: { gameId },
  })
}

export async function upsertRequirement(key: RequirementKey, requirement: Requirement) {
  await prisma.requirement.upsert({
    where: { gameId_id: key },
    create: requirement,
    update: requirement,
    select: null,
  })
}

export async function deleteRequirement(key: RequirementKey) {
  await prisma.requirement.delete({
    where: { gameId_id: key },
    select: null,
  })
}
