import { createHash } from 'crypto'
import { File } from '@prisma/client'
import { prisma } from './client'

export { File }
export type FileKey = Pick<File, 'hash'>

export async function storeFile(file: File): Promise<File> {
  return await prisma.file.upsert({
    where: {
      hash: file.hash,
    },
    create: file,
    update: {},
  })
}

export async function getFile(hash: string): Promise<File | null> {
  return await prisma.file.findUnique({
    where: { hash },
  })
}

export function hashBuffer(buffer: Buffer) {
  const hasher = createHash('sha256')
  hasher.setEncoding('hex')
  hasher.write(buffer)
  hasher.end()
  return hasher.read() as string
}
